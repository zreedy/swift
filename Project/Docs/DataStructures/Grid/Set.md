## method Set ##

### Description ###
	void Set(number x, number y, variant value)
Set the value at (`x`, `y`) in the array to `value`.

### Parameter List ###
#### x ####
> X offset. Can be a double but will lose precision due to being casted.

#### y ####
> Y offset. Can be a double but will lose precision due to being casted.

#### value ####
> Value to set. Can be of any type.

### Return Values ###
This method returns `nil`.