## method Get ##

### Description ###
	variant Get(number x, number y)
This method will return the value stored at (`x`, `y`) in the 2d array.

### Parameter List ###
#### x ####
> X offset. Can be a double but it will lose precision due to being casted.

#### y ####
> Y offset. Can be a double but it will lose precision due to being casted.

### Return Values ###
The value stored at (`x`, `y`).