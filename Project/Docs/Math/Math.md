## class Math ##

### Description ###
The math class provides a lot of math functions ranging from basic operations to simplifying common algorithms.

**Note**: Most of the methods in this class accept a special kind of variant called "number" which means they will take both an integer and a double. Usually these methods will only return a double, sometimes they will return the same type as the input. *Be careful and avoid bugs by reading the type returned and how types are treated by each method.*

### Methods ###
| | Method Name | Description |
|-|------------ | ------------|
|$| [Abs(var x)](/Math/Math/Abs) | Returns the absolute value. |
|$| [Acos(var x)](/Math/Math/Acos) | Returns the arc cosine value. |
|$| [Asin(var x)](/Math/Math/Asin) | Returns the arc sine value. |
|$| [Atan(var x)](/Math/Math/Atan) | Returns the arc tangent value. |
|$| [Atan2(var x, var y)](/Math/Math/Atan2) | Returns the arc tangent value. |
|$| [Ceil(var x)](/Math/Math/Ceil) | Round decimal up to whole number. |
|$| [Clamp(var x, var min, var max)](/Math/Math/Clamp) | Returns a value clamped in a range. |
|$| [Cos(var x)](/Math/Math/Cos) | Returns the cosine value. |
|$| [Cosh(var x)](/Math/Math/Cosh) | Returns the hyperbolic cosine value. |
|$| [Exp(var x)](/Math/Math/Exp) | Returns **e** raised to a power. |
|$| [Floor(var x)](/Math/Math/Floor) | Round decimal down to whole number. |
|$| [Lerp(var a, var b, var w)](/Math/Math/Lerp) | Linear interpolation. |
|$| [Log(var x)](/Math/Math/Log) | Logarithmic value. |
|$| [Log10(var value)](/Math/Math/Log10) | Base 10 logarithm value. |
|$| [Max(var x, var y)](/Math/Math/Max) | Return largest argument passed. |
|$| [Min(var x, var y)](/Math/Math/Min) | Return smallest argument passed. |
|$| [Pow(var n, var e)](/Math/Math/Pow) | Return n raised to e. |
|$| [RLerp(var a, var b, var w)](/Math/Math/RLerp) | Reverse linear interpolation |
|$| [Round(var x)](/Math/Math/Round) | Return decimal rounded to nearest whole number. |
|$| [Sign(var x)](/Math/Math/Sign) | Returns the sign of a value. |
|$| [Sin(var x)](/Math/Math/Sin) | Returns the sine of a value. |
|$| [Sinh(var x)](/Math/Math/Sinh) | Returns the hyperbolic sine of a value. |
|$| [Sqrt(var x)](/Math/Math/Sqrt) | Returns the square root of a value. |
|$| [Tan(var x)](/Math/Math/Tan) | Returns the tangent of a value. |
|$| [Tanh(var x)](/Math/Math/Tanh) | Returns the hyperbolic tangent of a value. |
|$| [PointInRect(var px, var py, var rx, var ry, var w, var h)](/Math/Math/PointInRect) | Returns if a point is in a rectangle. |

### Constants ###
| | Constant Name | Description  |
|-|-------------- | -------------|
| | [Pi](/Math/Math/Pi) | Value of Pi. |
| | [E](/Math/Math/E) | Value of E. |
