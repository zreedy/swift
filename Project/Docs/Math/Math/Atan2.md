## method Atan2 $ ##

### Description ###
	double Atan2(number x, number y)
Returns the [arc tangent](http://en.wikipedia.org/wiki/Arc_tangent) of `x`/`y` in radians.

### Parameter List ###
#### x ####
> Value in radians

### y ####
> Value in radians

### Return Value ###
A double containing the result.