## method Ceil $ ##

### Description ###
	double Ceil(number x)
Returns the decimal place of a floating point number rounded up to the next nearest whole number.

### Parameter List ###
#### x ####
> Value

### Return Values ###
A double containing the result.