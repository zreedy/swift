## method Floor $ ##

### Description ###
	double Floor(number x)
Returns the decimal place of a floating point number rounded down to the next nearest whole number.

### Parameter List ###
#### x ####
> Value

### Return Values ###
A double containing the result.