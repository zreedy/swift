## method Acos $ ##

### Description ###
	double Acos(number x)
Returns the [arc cosine](http://en.wikipedia.org/wiki/Arc_cosine) of `x` in radians.

### Parameter List ###
#### x ####
> Value in radians

### Return Value ###
A double containing the result.