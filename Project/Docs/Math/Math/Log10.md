## method Log10 $ ##

### Description ###
	double Log10(number x)
Returns the [common logarithm](http://en.wikipedia.org/wiki/Common_log) of `x`.

### Parameter List ###
#### x ####
> Value

### Return Values ###
A double containing the result.