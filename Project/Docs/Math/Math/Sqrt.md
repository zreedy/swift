## method Sqrt $ ##

### Description ###
	double Sqrt(number x)
Returns the square root of `x`.

### Parameter List ###
#### x ####
> Value

### Return Values ###
A double containing the result of the operation.