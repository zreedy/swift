## method Cos $ ##

### Description ###
	double Cos(number x)
Returns the [cosine](http://en.wikipedia.org/wiki/Cosine) of `x` in radians.

### Parameter List ###
#### x ####
> Value in radians

### Return Values ###
A double containing the result.