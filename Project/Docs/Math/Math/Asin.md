## method Asin $ ##

### Description ###
	double Asin(number x)
Returns the [arc sine](http://en.wikipedia.org/wiki/Arc_sine) of `x` in radians.

### Parameter List ###
#### x ####
> Value in radians

### Return Value ###
A double containing the result.