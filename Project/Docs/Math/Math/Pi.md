## const Pi ##

### Description ###
	const Pi
Represents the ratio of the circumference of a circle to its diameter, specified by the const, &pi;.

### Value ###
	3.1415926535897931