## method Atan $ ##

### Description ###
	double Atan(number x)
Returns the [arc tangent](http://en.wikipedia.org/wiki/Arc_tangent) of `x` in radians.

### Parameter List ###
#### x ####
> Value in radians

### Return Value ###
A double containing the result.