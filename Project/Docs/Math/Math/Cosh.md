## method Cosh $ ##

### Description ###
	double Cosh(number x)
Returns the [hyperbolic cosine](http://en.wikipedia.org/wiki/Hyperbolic_function) of `x` in radians.

### Parameter List ###
#### x ####
> Value in radians

### Return Values ###
A double containing the result.