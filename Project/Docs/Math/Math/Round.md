## method Round $ ##

### Description ###
	double Round(number x)
Returns `x` rounded to the nearest whole number with 0.5 rounding up.

### Parameter List ###
#### x ####
> Value to round

### Return Values ###
A double containing the result of the operation.