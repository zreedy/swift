## method Lerp $ ##

### Description ###
	double Lerp(number a, number b, number w)
Returns the [linear interpolation](http://en.wikipedia.org/wiki/Linear_interpolation) of `a` to `b` given `w`.

### Parameter List ###
#### a ####
> First value.

#### b ####
> Second value.

#### w ####
> Factor

### Return Values ###
A double containing the result of the operation.