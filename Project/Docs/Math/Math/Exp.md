## method Exp $ ##

### Description ###
	double Exp(number x)
Returns **e** raised to `x` (**e**^`x`).

### Parameter List ###
#### x ####
> Exponent to raise **e** to.

### Return Values ###
A double containing the result.