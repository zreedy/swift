## method Tan $ ##

### Description ###
	double Tan(number x)
Returns the [tangent](http://en.wikipedia.org/wiki/Tangent_function) of `x` in radians.

### Parameter List ###
#### x ####
> Value in radians

### Return Value ###
A double containing the result.