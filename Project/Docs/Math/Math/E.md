## const E ##

### Description ###
	const E
Represents the natural logarithmic base, specified by the constant, **e**.

### Value ###
	2.7182818284590451