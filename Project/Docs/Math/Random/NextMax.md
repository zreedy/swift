## method Next $ ##

### Description ###
	double Next(var max)
This method returns a random positive whole number between 0 (inclusive) and the value of `max` (exclusive).

### Parameter List ###
#### max ####
> The exclusive maximum limit for the random number.

### Return Values ###
A double containing a random positive whole number.