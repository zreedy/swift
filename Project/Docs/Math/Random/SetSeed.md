## method SetSeed $ ##

### Description ###
	void SetSeed(int value)
Sets the underlying RNG's seed to `value`.

### Parameter List ###
#### value ####
> Value to set the seed to.

### Return Values ###
This method returns nil.