## method Next $ ##

### Description ###
	double Next()
This method returns a random positive whole number.

### Parameter List ###
> *None*

### Return Values ###
A double containing a random positive whole number.