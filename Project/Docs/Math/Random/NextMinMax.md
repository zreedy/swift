## method Next $ ##

### Description ###
	double Next(var min, var max)
This method returns a random positive whole number between the value of `min` (inclusive) and the value of `max` (exclusive).

### Parameter List ###
#### min ####
> The inclusive minimum limit for the random number.

#### max ####
> The exclusive maximum limit for the random number.

### Return Values ###
A double containing a random positive whole number.