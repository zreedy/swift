## property Status ##

### Description ###
```swift
Sound.Status Status { get; private set; }
```
Status is a **read-only** property that describes the current [playing state](/Audio/Sound/StatusEnum) of the sound.