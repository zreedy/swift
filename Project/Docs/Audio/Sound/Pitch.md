## property Pitch ##

### Description ###
```swift
double Pitch { get; set; }
```
Pitch is a read-write property that describes the pitch of the sound when played. This property is a double normalized at 1.0 (1.0 == normal pitch).