## method Play ##

### Description ###
```swift
void Play()
```
This method will change the [playing state](/Audio/Sound/StatusEnum) of the sound to playing. The sound will be played once and when the sound is finished it will change its state to stopped. This will always reset the position of the sound to 0ms before playing.

### Parameters ###
>*None*

### Return Values ###
This method will always return nil.