## field b ##

### Description ###
	int b
This field represents the blue component of the color. This field should be clamped between 0 and 255 (integer).