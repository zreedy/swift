## field a ##

### Description ###
	int a
This field represents the alpha component of the color. This field should be clamped between 0 and 255 (integer).