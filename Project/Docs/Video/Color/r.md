## field r ##

### Description ###
	int r
This field represents the red component of the color. This field should be clamped between 0 and 255 (integer).