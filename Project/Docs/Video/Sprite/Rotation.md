## field Rotation ##

### Description ###
	double Rotation
This field describes the rotation in degrees around the origin point of the sprite that the sprite will be drawn with. If `Rotation` is larger than 360 or less than 0 then it will loop back around to clamp between 0 and 360 internally. 