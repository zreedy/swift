## field Animated ##

### Description ###
	bool Animated
This field flags if the sprite is currently running through animation. By default it is set to `true`, even for textures with just one frame.