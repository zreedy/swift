## field ScaleX ##

### Description ###
	double ScaleX
This field describes the horizontal (x-axis) scale of the sprite. By default this field is initialized to 1.0. A negative 

A scale of 2.0 would mean that the sprite is drawn at twice its original size horizontally.