## method Draw ##

### Description ###
	void Draw()
This method will draw the sprite with all defaults used. That means it will be drawn at the position specified by ([`x`](/Video/Sprite/x), [`y`](/Video/Sprite/y)) with the [blend color](/Video/Sprite/Color), [x-scale](/Video/Sprite/ScaleX), [y-scale](/Video/Sprite/ScaleY) and [rotation](/Video/Sprite/Rotation) being taken into account.

This method will also update the animation information as described in [`FrameSpeed`](/Video/Sprite/FrameSpeed).