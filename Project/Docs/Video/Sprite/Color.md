## field Color ##

### Description ###
	Color Color
This field describes the current blend color of the sprite. By default it is initialized to `null`. If this field is `null` then the sprite will be blended with white (as in no blend color being used).

For more information see the [`Color`](/Video/Color) class.