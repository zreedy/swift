## class Time ##

### Description ###
The `Time` class provides an interface for various time and timing related functions.

### Properties ###
| | Property Name | Description |
|-|-------------- | ------------|
|$%g%s| [Timestamp](/System/Time/Timestamp) | Current UNIX timestamp. |

