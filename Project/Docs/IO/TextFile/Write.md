## method Write ##

### Description ###
	void Write(string value)
This method writes the string `value` to the file.

### Parameter List ###
#### value ####
>The string to write to the file.

### Return Values ###
This method returns `nil`.