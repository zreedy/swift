## method ReadToEnd ##

### Description ###
	string ReadToEnd()
This method reads from the current cursor's position to the end of the file and returns the data read a string.

### Parameter List ###
>*None*

### Return Values ###
The contents from the cursor's position to the end of the file in a string.