## enum Button ##

### Description ###
```swift
enum Button
```
This enum describes which button on the mouse is being referenced. Mouse buttons 4 (Fourth) through 8 (Eighth) are typically not found on stock mice. These are more common on office or gaming mice. These serve a variety of functions such as "forward" and "back" buttons or increasing/decreasing mouse sensitivity. These buttons may be mapped through the device's driver software to other actions that may interfere with the game. Be cautious when relying on these buttons, their use will vary from computer to computer.

### Values ###
| | Member Name | Description                         |
|-|------------ | ------------------------------------|
| | Left        | The left mouse button               |
| | Right       | The right mouse button              |
| | Middle      | The middle mouse button             |
| | First       | The first (**left**) mouse button   |
| | Second      | The second (**right**) mouse button |
| | Third       | The third (**middle**) mouse button |
| | Fourth      | The fourth mouse button             |
| | Fifth       | The fifth mouse button              |
| | Sixth       | The sixth mouse button              |
| | Seventh     | The seventh mouse button            |
| | Eighth      | The eighth mouse button             |
