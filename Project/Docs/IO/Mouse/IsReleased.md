## method IsReleased ##

### Description ###
	bool IsReleased(Mouse.Button button)
This method returns if a button on the mouse was just released by the user.

### Parameters ###
#### button ####
> [Mouse button](/IO/Mouse/Button) to check.

### Return Values ###
Returns true if the mouse button was just released. Otherwise false is returned.