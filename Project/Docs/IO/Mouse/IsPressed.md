## method IsPressed ##

### Description ###
	bool IsPressed(Mouse.Button button)
This method returns if a button on the mouse was just pressed by the user.

### Parameters ###
#### button ####
> [Mouse button](/IO/Mouse/Button) to check.

### Return Values ###
Returns true if the mouse button was just pressed. Otherwise false is returned.