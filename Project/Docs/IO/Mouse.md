## class Mouse ##

### Description ###
The `Mouse` class provides an interface to the mouse.

### Properties ###
| | Property Name | Description |
|-|-------------- | ------------|
|$| [X](/IO/Mouse/X) | Cursor X position. |
|$| [Y](/IO/Mouse/Y) | Cursor Y position. |

### Methods ###
| | Method Name | Description |
|-|------------ | ------------|
|$| [IsPressed(var key)](/IO/Mouse/IsPressed) | Check if a button was just pressed. |
|$| [IsHeld(var key)](/IO/Mouse/IsHeld) | Check if a button is being held down. |
|$| [IsReleased(var key)](/IO/Mouse/IsReleased) | Check if a button was just released. |

### Enumerations ###
| | Enum Name | Description |
|-|---------- | ------------|
| | [Button](/IO/Mouse/Button) | Mouse button. |