## method IsHeld ##

### Description ###
	bool IsHeld(Keyboard.Key key)
This method returns if a key on the keyboard is currently being held down by the user.

### Parameters ###
#### key ####
>[Keyboard key](/IO/Keyboard/Key) to check.

### Return Values ###
Returns true if the key is currently being held down. Otherwise false is returned.