## method IsReleased ##

### Description ###
	bool IsReleased(Keyboard.Key key)
This method returns if a key on the keyboard was just released by the user.

### Parameters ###
#### key ####
>[Keyboard key](/IO/Keyboard/Key) to check.

### Return Values ###
Returns true if the key was just released. Otherwise false is returned.