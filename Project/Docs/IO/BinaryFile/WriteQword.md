## method WriteQword ##

### Description ###
	void WriteQword(int value)
This method writes 8 bytes of `value` to the file and advances the cursor.

### Parameter List ###
#### value ####
>Value to write to the file.

### Return Values ###
This method returns `nil`.