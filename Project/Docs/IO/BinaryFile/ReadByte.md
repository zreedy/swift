## method ReadByte ##

### Description ###
	int ReadByte()
This method will read 1 byte from the file and advance the cursor.

### Parameter List ###
>*None*

### Return Values ###
The value of the data read from the file.