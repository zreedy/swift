## method ReadWord ##

### Description ###
	int ReadWord()
This method will read 2 bytes from the file and advance the cursor.

### Parameter List ###
>*None*

### Return Values ###
The value of the data read from the file.