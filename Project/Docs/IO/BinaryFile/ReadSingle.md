## method ReadSingle ##

### Description ###
	double ReadSingle()
This method will read a single precision floating point number (4 bytes) from the file and advance the cursor.

### Parameter List ###
>*None*

### Return Values ###
The value of the data read from the file.