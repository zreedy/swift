## property Mode ##

### Description ###
	BinaryFile.Mode Mode { get; private set; }
This read-only property returns the [mode](/IO/BinaryFile/ModeEnum) that the file was opened in.