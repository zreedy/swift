## method WriteDouble ##

### Description ###
	void WriteDouble(double value)
This method writes a double precision floating point number (8 bytes) of `value` to the file and advances the cursor.

### Parameter List ###
#### value ####
>Value to write to the file.

### Return Values ###
This method returns `nil`.