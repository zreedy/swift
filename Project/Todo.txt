Todo
====

Overall
-------
[X] Sounds
[ ] Fonts
[X] Scripting
[X] States
[X] Levels


Engine
------
[X] Backgronds
[X] Tilesets
[ ] Primitive shapes
[ ] Text/Fonts
[X] Levels
[ ] Surfaces
[X] Virtual Machine
[X] Events
[ ] EventTimer
[X] Logger
[X] Thread window so that games don't freeze when being moved
[X] Engine version tool
[X] Refactor code to use Ns::ClassSingleton/Class style
[X] Audio engine failing to initialize should not be fatal

Compiler
--------
[X] Script Compiler
[ ] Verbose Logger

Misc
----
[X] Split sound data from audio data in GD
[X] Verify that depth sorting works
[X] Entity discovery (w/ tags and InArea)
[X] VM objects
[X] Fully linked entity/state objects
[X] Garbage Collection
[X] Origins don't work
