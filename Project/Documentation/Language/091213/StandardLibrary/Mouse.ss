/*
 *	Mouse.spl
 *	Revision September 12th, 2013
 *
 *	Standard Library Class "Mouse".
 */

global Mouse;

/*
 *	IsPressed
 *	IsDown
 *	IsReleased
 *	GetScrollDelta
 *	Enum.Buttons
 */
