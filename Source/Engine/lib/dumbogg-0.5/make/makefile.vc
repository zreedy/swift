.PHONY: all lib install uninstall clean veryclean

include make/makefile.lst

OPT = -G5

DEBUG_OBJDIR = obj/msvc/debug
RELEASE_OBJDIR = obj/msvc/release
LIBDIR = lib/msvc

ifdef DEBUGMODE
	FLAGS = -D"DEBUGMODE" -MDd -W3 -ZI -GZ
	MINFLAGS = -MDd
	LIBS = vorbisfile.lib vorbis.lib ogg.lib aldmd.lib dumbd.lib alld.lib
	OBJDIR = $(DEBUG_OBJDIR)
	LIB_FILE = $(LIBDIR)/$(LIB_DEBUG).lib
else
	FLAGS = -MD -W3 -Ogityb2 -Gs $(OPT)
	MINFLAGS = -MD
	LIBS = vorbisfile.lib vorbis.lib ogg.lib aldmb.lib dumb.lib alleg.lib
	OBJDIR = $(RELEASE_OBJDIR)
	LIB_FILE = $(LIBDIR)/$(LIB).lib
endif


#--------------------------------------#
#-- Check for environment variables ---#

.PHONY: badwin badmsvc badspaces

ifeq ($(OS),Windows_NT)
WINSYSDIR = $(SYSTEMROOT)
WINSUBDIR = system32

ifeq ($(WINSYSDIR),)        # Fix for Win2k
WINSYSDIR = $(SystemRoot)
endif

else
WINSYSDIR = $(windir)
WINSUBDIR = system
endif

ifneq ($(WINSYSDIR),)
WINDIR_U = $(subst \,/,$(WINSYSDIR)/$(WINSUBDIR))
WINDIR_D = $(subst /,\,$(WINSYSDIR)/$(WINSUBDIR))
else
badwin:
	@echo Your SYSTEMROOT or windir environment variable is not set!
endif


ifdef MSVCDir
MSVCDIR_U = $(subst \,/,$(MSVCDir))
MSVCDIR_D = $(subst /,\,$(MSVCDir))
else
ifdef MSDevDir
MSVCDIR_U = $(subst \,/,$(MSDevDir))
MSVCDIR_D = $(subst /,\,$(MSDevDir))
else
ifdef MSVCDIR
MSVCDIR_U = $(subst \,/,$(MSVCDIR))
MSVCDIR_D = $(subst /,\,$(MSVCDIR))
else
ifdef MSDEVDIR
MSVCDIR_U = $(subst \,/,$(MSDEVDIR))
MSVCDIR_D = $(subst /,\,$(MSDEVDIR))
else
badmsvc:
	@echo Your MSVCDIR or MSDEVDIR environment variable is not set!
endif
endif
endif
endif

NULLSTRING :=
SPACE := $(NULLSTRING) # special magic to get an isolated space character

ifneq ($(findstring $(SPACE),$(MSVCDIR)$(MSDEVDIR)),)
badspaces:
	@echo There are spaces in your MSVCDIR or MSDEVDIR environment
	@echo variables: please change these to the 8.3 short filename
	@echo version, or move your compiler to a different directory.
endif


OBJDIR_D = $(subst /,\,$(OBJDIR))
OBJECTS = $(addprefix $(OBJDIR)/, $(notdir $(patsubst %.c, %.obj, $(MODULES))))
EXAMPLES_EXE = $(addprefix examples/, $(notdir $(patsubst %.c, %.exe, $(EXAMPLES))))

RUNNER = obj\runner.exe

all: $(LIB_FILE) $(EXAMPLES_EXE)

lib: $(LIB_FILE)

clean:
	- del $(subst /,\,$(DEBUG_OBJDIR))\*.obj
	- del $(subst /,\,$(RELEASE_OBJDIR))\*.obj
	- del examples\*.obj

veryclean: clean
	- del $(subst /,\,$(LIB_FILE))

install: $(LIB_FILE) include/dumbogg.h
	xcopy $(subst /,\,$(LIB_FILE)) $(MSVCDIR_D)\lib
	xcopy include\dumbogg.h $(MSVCDIR_D)\include

uninstall:
	- del $(MSVCDIR_D)\include\dumbogg.h
	- del $(MSVCDIR_D)\lib\$(notdir $(LIB_FILE))

$(RUNNER): src/misc/runner.c
	gcc -O -Wall -Werror -o $(RUNNER) src/misc/runner.c

$(OBJDIR)/%.obj: src/%.c include/dumbogg.h $(RUNNER)
	$(RUNNER) cl -nologo @ -c $< -Iinclude -Fo$@ $(FLAGS)

examples/%.obj: examples/%.c include/dumbogg.h $(RUNNER)
	$(RUNNER) cl -nologo @ -c $< -Iinclude -Fo$@ $(FLAGS)

examples/%.exe: examples/%.obj $(RUNNER) $(LIB_FILE)
	$(RUNNER) cl -nologo @ $< -Iinclude -Fe$@ $(FLAGS) $(LIB_FILE) $(LIBS)

$(LIB_FILE): $(OBJECTS) $(RUNNER)
	$(RUNNER) lib -nologo @ -out:$(LIB_FILE) $(OBJECTS)

