# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  "RC"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/adler32.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/adler32.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/compress.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/compress.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/crc32.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/crc32.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/deflate.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/deflate.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/gzclose.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/gzclose.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/gzlib.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/gzlib.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/gzread.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/gzread.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/gzwrite.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/gzwrite.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/infback.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/infback.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/inffast.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/inffast.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/inflate.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/inflate.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/inftrees.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/inftrees.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/trees.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/trees.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/uncompr.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/uncompr.obj"
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/zutil.c" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/zutil.obj"
  )
SET(CMAKE_C_COMPILER_ID "MSVC")
SET(CMAKE_DEPENDS_CHECK_RC
  "X:/Swift/Source/Engine/lib/zlib-1.2.8/win32/zlib1.rc" "X:/Swift/Source/Engine/lib/zlib-1.2.8/build-release/CMakeFiles/zlib.dir/win32/zlib1.res"
  )

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "NO_FSEEKO"
  "_CRT_SECURE_NO_DEPRECATE"
  "_CRT_NONSTDC_NO_DEPRECATE"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )
