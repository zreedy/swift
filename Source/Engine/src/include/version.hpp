/*
 *	version.hpp
 *	Swift Engine Version
 *	Updated by the engine post-build tool
 */

#ifndef __ENGINE_VERSION_HPP
#define __ENGINE_VERSION_HPP

#define ENGINE_VERSION_MAJOR 0
#define ENGINE_VERSION_MINOR 13
#define ENGINE_VERSION_BUILD 261
#ifndef DEBUG
#define ENGINE_VERSION_REV_TAG "e67207e"
#else
#define ENGINE_VERSION_REV_TAG "dev"
#endif

#endif
