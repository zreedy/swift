/*
 *	stdlib.hpp
 *	Swift Standard Library
 */

#ifndef __STDLIB_STATIC_HPP
#define __STDLIB_STATIC_HPP

namespace StdLib
{
	void Install();
	void Uninstall();
}

#endif
