﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Swift Compiler")]
[assembly: AssemblyDescription("Swift's asset and source compiler")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("x2048")]
[assembly: AssemblyProduct("Swift")]
[assembly: AssemblyCopyright("2013 x2048")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c79bd325-8011-4458-b179-52f5f033a9ed")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("0.13.1539-e67207e")]
[assembly: NeutralResourcesLanguage("en-US")]
